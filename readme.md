

openssl genrsa -aes128 -out alice_private.pem 1024 //generating the private

openssl rsa -in alice_private.pem -pubout > alice_public.pem //generating the public

// for bob and alice, and then they exchange the public keys to one another

openssl rsautl -encrypt -inkey bob_public.pem -pubin -in hola.txt -out hola.enc //alice encrypting with bob's public key

openssl rsautl -decrypt -inkey bob_private.pem -in ../alice/hola.enc -out hola.txt //bob decrypting with his private key
